export interface BalanceProps {
    income: number,
    expense: number
}

export default function Balance({expense,income}:BalanceProps ) {
    return (
        <>
            <h4>Your Balance</h4>
            <h1 id="balance">${(income) - -1 * (expense)}</h1>
        </>
    )
}
