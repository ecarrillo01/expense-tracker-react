import { Transaction } from '../store';

interface Props {
  transactions: Transaction[];
  onRemove(id: number): void;
}

export default function TransactionList({ onRemove, transactions }: Props) {
  return (
    <>
      <h3>History</h3>
      <ul id="list" className="list">
        {transactions.map((transaction) => (
          <li
            key={transaction.id}
            className={transaction.amount < 0 ? 'minus' : 'plus'}
          >
            {transaction.text}{' '}
            <span>
              {transaction.amount < 0 ? '-' : ''}${transaction.amount}
            </span>
            <button
              className="delete-btn"
              onClick={() => onRemove(transaction.id)}
            >
              x
            </button>
          </li>
        ))}
      </ul>
    </>
  );
}
