import { FormEvent, useState } from 'react'
import useStore from '../store';

export default function AddTransaction() {
    const store = useStore()
    const [text, setText] = useState<string>('');
    const [amount, setAmount] = useState<string>('');

    const addTransaction = (e: FormEvent<HTMLFormElement>) => {
        const target = e.target as HTMLFormElement;
        e.preventDefault()
        store.addTransaction({ amount: parseInt(amount), id: Date.now(), text });
        setText('');
        setAmount('');
        target['transaction-name'].focus();
    }
    return (<>
        <h3>Add new transaction</h3>
        <form id="form" onSubmit={addTransaction}>
            <div className="form-control">
                <label htmlFor="text">Text</label>
                <input id="transaction-name" value={text} onChange={(e) => setText(e.target.value)} type="text" placeholder="Enter name..." />
            </div>
            <div className="form-control">
                <label htmlFor="amount"
                >Amount <br />
                    (negative - expense, positive - income)</label
                >
                <input value={amount} onChange={(e) => setAmount(e.target.value)} type="number" placeholder="Enter amount..." />
            </div>
            <button type='submit' className="btn">Add transaction</button>
        </form>
    </>);
}