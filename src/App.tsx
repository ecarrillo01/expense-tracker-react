import AddTransaction from './components/AddTransaction';
import Balance from './components/Balance';
import Header from './components/Header';
import IncomeExpenses from './components/IncomeExpenses';
import TransactionList from './components/TransactionList';
import useTransactions from './store';

function App() {
  const { getIncome, getExpense, removeTransaction, transactions } =
    useTransactions();
  const income = getIncome();
  const expense = getExpense();

  return (
    <>
      <Header />
      <div className="container">
        <Balance income={income} expense={expense} />
        <IncomeExpenses income={income} expense={expense} />
        <TransactionList
          transactions={transactions}
          onRemove={removeTransaction}
        />
        <AddTransaction />
      </div>
    </>
  );
}

export default App;
