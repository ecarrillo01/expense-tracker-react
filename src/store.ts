import { create } from 'zustand';
import { persist, createJSONStorage } from 'zustand/middleware';

export interface Transaction {
  id: number;
  text: string;
  amount: number;
}

type Store = {
  transactions: Transaction[];
  setTransactions: (newTransactions: Transaction[]) => void;
  addTransaction: (newTransaction: Transaction) => void;
  removeTransaction: (id: number) => void;
  getIncome: () => number;
  getExpense: () => number;
};

export const useTransactions = create(
  persist<Store>(
    (set, get) => ({
      transactions: [],
      setTransactions: (newTransactions) =>
        set(() => ({ transactions: newTransactions })),
      addTransaction: (newTransaction) =>
        set((state) => ({
          transactions: state.transactions.concat(newTransaction),
        })),
      removeTransaction: (id) =>
        set((state) => ({
          transactions: state.transactions.filter((item) => item.id !== id),
        })),
      getIncome: () =>
        get()
          ?.transactions.filter((item) => item.amount > 0)
          .reduce((prev, curr) => prev + curr.amount, 0) ?? 0,
      getExpense: () =>
        get()
          ?.transactions.filter((item) => item.amount < 0)
          .reduce((prev, curr) => prev + curr.amount, 0) ?? 0,
    }),
    {
      name: 'transactions-storage',
      storage: createJSONStorage(() => localStorage),
    }
  )
);

export default useTransactions;
